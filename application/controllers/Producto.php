<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Producto extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Producto_model');
        $this->load->helper('url_helper');
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
        //cabeceras de respuesta JSON
		header('Content-type: application/json');
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");

        $status_header;
        $respuesta;
        //manejo de errores
        set_error_handler(array($this, 'errores_rest'));
        //filtrado de petición
    	$stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
	    
        $request = json_decode($stream_clean);
	    $producto = $this->Producto_model;

        //datos material
	    $material = $request->MT_MM020_REQ->Producto;
        
        $producto->codMaterial = trim($material->cod_material,"0");
        $producto->codAnterior = trim($material->cod_anterior,"0");

        //cargo sociedades
        foreach ($material->Sociedad as $sociedad)
        {
            if(isset($sociedad->afecto))
            {
                $afecto = $sociedad->afecto;
            }
            else
            {
                $afecto = null;
            }
            $producto->addSociedad(trim($sociedad->cod_material,"0"), $sociedad->org_ventas, $sociedad->canal_dist, $sociedad->uni_venta,
                                   $sociedad->costo_medio_variable, $sociedad->costo_comercial, $sociedad->des_peru, $sociedad->a_pedido,
                                   $sociedad->grupo_material, $sociedad->marca_propia, $sociedad->importado, $afecto, $sociedad->cat_comision, $sociedad->cod_neg);
        }
        //cargo centros
        foreach ($material->Centros as $centro)
        {
            $producto->addCentro($centro->cod_centro, $centro->rotacion);
        }
        //impuestos
        foreach($material->Impuesto as $impuesto)
        {
            $producto->addImpuesto($impuesto->pais, $impuesto->imp_especifico, $impuesto->clasif_fiscal);
        }
        var_dump($producto);
        die;
        //grabo orden
	    $producto->InsertProducto();
        //genero respuesta exitosa
        $respuesta = array("COD_MSG" => 0,
                            "MSG" => "Exitoso");
        $status_header = 200;

        $this->load->model('LogRequest_model');
        //grabo log de peticiones
        $this->LogRequest_model->estado = 0;
        $this->LogRequest_model->xmlEntrada = $stream_clean;
        $this->LogRequest_model->insertLog();

        //envío respuesta
    	return $this->output
                ->set_content_type('application/json')
                ->set_status_header($status_header)
                ->set_output(json_encode($respuesta));
	}

	function errores_rest($errno, $errstr, $errfile, $errline)
	{
        $this->load->model('LogRequest_model');
        header('Content-type: application/json');
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");

        $mensaje = $errno.'-'.$errstr.'-'.$errfile.'-'.$errline;

        //$ci->guardaXML(file_get_contents("php://input"),-4);

        //grabo log de peticiones
        $this->LogRequest_model->estado = 4;
        $this->LogRequest_model->xmlEntrada = file_get_contents("php://input");
        $this->LogRequest_model->insertLog();
        //envío respuesta
        $respuesta = array("COD_MSG" => 4,
                            "MSG" => $mensaje);
        $status_header = 500;
      	echo json_encode($respuesta);
        die;
     }
}