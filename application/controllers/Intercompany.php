<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Intercompany extends CI_Controller {

	public function __construct()
        {
            parent::__construct();
            $this->load->model('Intercompany_model');
            $this->load->helper('url_helper');
        }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
        //cabeceras de respuesta JSON
		header('Content-type: application/json');
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");

        $status_header;
        $respuesta;
        //manejo de errores
        set_error_handler(array($this, 'errores_rest'));
        //filtrado de petición
    	$stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
	    
        $request = json_decode($stream_clean);
	    $ordIntercompany = $this->Intercompany_model;

        //de_intercompany
	    $materiales = $request->MT_MM007_REQ_IN->Posicion;
        //en_intercompany
	    $ordIntercompany->numPed = $request->MT_MM007_REQ_IN->Cabecera->NUMERO_PEDIDO;
	    $ordIntercompany->codProveedor = ltrim($request->MT_MM007_REQ_IN->Cabecera->COD_PROVEEDOR,"0");
        if($request->MT_MM007_REQ_IN->Cabecera->COD_SOCIEDAD != 2000)
        {
            $ordIntercompany->rutProveedor = substr(ltrim($request->MT_MM007_REQ_IN->Cabecera->RUT_PROVEEDOR."0"), 0,8);
        }
        else
        {
            $ordIntercompany->rutProveedor = $request->MT_MM007_REQ_IN->Cabecera->RUT_PROVEEDOR;
        }
	    
	    $ordIntercompany->codSociedad = $request->MT_MM007_REQ_IN->Cabecera->COD_SOCIEDAD;

        //arreglo por si viene solo un producto y no tener que hacer mil ifs
        if(isset($request->MT_MM007_REQ_IN->Posicion->PO_ITEM))
        {
          $materiales = array($request->MT_MM007_REQ_IN->Posicion);
        }
        //llenado de_intercompany
	    foreach ($materiales as $material) {
            
	    	$ordIntercompany->addMaterial($material->PO_ITEM, ltrim($material->COD_MATERIAL,"0"), $material->CANTIDAD,
	    								  $material->UNID_CANTIDAD,$material->PRECIO_NETO, $material->MONEDA);
	    }
        //grabo orden
	    $ordIntercompany->InsertOrder();
        //genero respuesta exitosa
        $respuesta = array("COD_MSG" => 0,
                            "MSG" => "Exitoso",
                            "NUMERO_PEDIDO_INTERCOMPANY" => $ordIntercompany->numPed);
        $status_header = 200;

       /* $this->load->model('LogRequest_model');
        //grabo log de peticiones
        $this->LogRequest_model->estado = 0;
        $this->LogRequest_model->xmlEntrada = $stream_clean;
        $this->LogRequest_model->insertLog();
        */
        guardaLogBruto($stream_clean, 0);
        //envío respuesta
    	return $this->output
                ->set_content_type('application/json')
                ->set_status_header($status_header)
                ->set_output(json_encode($respuesta));
	}

	function errores_rest($errno, $errstr, $errfile, $errline)
	{
        //$this->load->model('LogRequest_model');
        header('Content-type: application/json');
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");

        $mensaje = $errno.'-'.$errstr.'-'.$errfile.'-'.$errline;

        //$ci->guardaXML(file_get_contents("php://input"),-4);

        //grabo log de peticiones
        /*$this->LogRequest_model->estado = 4;
        $this->LogRequest_model->xmlEntrada = file_get_contents("php://input");
        $this->LogRequest_model->insertLog();*/
        guardaLogBruto(file_get_contents("php://input"), 4);

        //envío respuesta
        $respuesta = array("COD_MSG" => 4,
                            "MSG" => $mensaje,
                            "NUMERO_PEDIDO_INTERCOMPANY" => 0);
        $status_header = 500;
      	echo json_encode($respuesta);
        die;
     }
}