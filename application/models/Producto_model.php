<?php
    require_once 'UMB.php';
    require_once 'Embalaje.php';
    require_once 'SubEmbalaje.php';
    require_once 'Medidas.php';
    require_once 'Sociedad.php';
    require_once 'Centro.php';
    require_once 'Impuesto.php';
class Producto_model extends CI_Model
{
    var $codMaterial = 0;
    var $codAnterior = "";
    var $tipoProd = "";
    var $desCorta = "";
    var $desLarga = "";
    var $codJerarquia = "";
    var $codLinea = 0;
    var $deslinea = "";
    var $codSeccion = 0;
    var $desSeccion = "";
    var $codRubro = 0;
    var $desRubro = "";
    var $codSubRubro = 0;
    var $desSubRubro = "";
    var $codGrupo = 0;
    var $desGrupo = "";
    var $codMarca = 0;
    var $desMarca = "";
    var $pesoBruto = 0;
    var $pesoNeto = 0;
    var $uniPeso = 0;
    var $volumen = 0;
    var $uniVolumen = "";
    var $uniBase = "";
    var $uniEmbalaje = "";
    var $denConvEmb = 0;
    var $numConvEmb = 0;
    var $pesoNetoEmbalaje = 0;
    var $pesoBrutoEmbalaje = 0;
    var $uniSubEmbalaje = "";
    var $denConvSubEmb = "";
    var $numConvSubEmb = 0;
    var $pesoNetoSubEmb = 0;
    var $pesoBrutoSubEmb = 0;
    var $UMB;
    var $embalaje;
    var $subEmbalaje;
    var $sociedades = array();
    var $impuestos = array();
    var $medidas;
    var $centros = array();

    public function __construct()
    {
        parent::__construct();
        $this->UMB = new UMB();
        $this->embalaje = new Embalaje();
        $this->subEmbalaje = new SubEmbalaje();
        $this->medidas = new Medidas();
    }

    public function insertProducto()
    {
        $dbConn;
        //seleccción de bd
        if($this->codSociedad == 2000)
        {
            $dbConn = "dmcCLTEST";
        }
        elseif ($this->codSociedad == 2200)
        {
            $dbConn = "dmcPRTEST";
        }
        else
        {
            $dbConn = "uniTEST";
        }
        $this->load->database($dbConn);

        //comprobación si producto existe
        $textSql = "SELECT COUNT(*) MATERIALEXISTE FROM MA_PRODUCT
                    WHERE CODSAP = ?";
        $query = $this->db->query($textSql,array($this->codMaterial));

        $materialExiste = $query->result()[0]->MATERIALEXISTE;
        echo $materialExiste;
        die;
        if($pedidoExiste == 0) //si el pedido no existe
        {
            //inicio transacción
            $this->db->trans_start();
            //particularidades dimerc
            if($this->codSociedad != 2200) //grabo cosprom para todas las sociedades salvo Dimerc Perú
            {
                if($this->codSociedad == 2000)
                {
                    //rol aplicación para grabar, sólo Dimerc Chile
                    $textSql = "SET ROLE ROL_APLICACION";
                    $this->db->query($textSql);
                }
                $sqlDetalle = "insert into de_intercompany(SAP_SOC_EMISOR, SAP_PED_INTER, SAP_ITM_PROD, SAP_COD_PROD, SAP_CAN_VTA, SAP_UND_VTA, SAP_PRE_VTA, SAP_MON_VTA,SAP_COSPROM)
                        values(?,?,?,?,?,?,?,?,?)";
            }
            else
            {
                $sqlDetalle = "insert into de_intercompany(SAP_SOC_EMISOR, SAP_PED_INTER, SAP_ITM_PROD, SAP_COD_PROD, SAP_CAN_VTA, SAP_UND_VTA, SAP_PRE_VTA, SAP_MON_VTA)
                        values(?,?,?,?,?,?,?,?)";
            }

            
            foreach ($this->materiales as $material)
            {
                if($this->codSociedad != 2200) //grabo cosprom para todas las sociedades salvo Dimerc Perú
                {
                    $textSql = "SELECT NVL(MAX(A.cosprom),0) COSPROM
                                from ma_costosxcentros a, ma_product b
                                where a.codpro = b.codpro
                                and b.codsap = ?";
                    $query = $this->db->query($textSql,array($material->codMaterial));
                    $cosprom = $query->result()[0]->COSPROM;
                    $arrInsert = array($this->codProveedor, $this->numPed,
                                                         $material->itmProd, $material->codMaterial,
                                                         $material->cantidad, $material->unidCantidad,
                                                         $material->precioNeto, $material->moneda,$cosprom);
                }
                else
                {
                    $arrInsert = array($this->codProveedor, $this->numPed,
                                                         $material->itmProd, $material->codMaterial,
                                                         $material->cantidad, $material->unidCantidad,
                                                         $material->precioNeto, $material->moneda);
                }
                $query = $this->db->query($sqlDetalle, $arrInsert);
            }

            $textSql = "SELECT NVL(CODEMP,0) CODEMP FROM MA_EMPRESA
                        WHERE SAP_SOCIEDAD = ?";
            $query = $this->db->query($textSql,array($this->codSociedad));
            $codemp = $query->result()[0]->CODEMP;

            $sqlCabecera = "insert into en_intercompany(SAP_PED_INTER,SAP_SOC_EMISOR,SAP_RUT_PROVE,SAP_SOC_DESTINO,LEG_CODEMP)
                        values(?,?,?,?,?)";
            $query = $this->db->query($sqlCabecera,array($this->numPed, $this->codProveedor, $this->rutProveedor, $this->codSociedad, $codemp));

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
            }
            else
            {
                $this->db->trans_commit();
            }
            $this->db->close();
            unset($this->db);
        }
        else
        {
            $this->db->close();
            unset($this->db);
        }
    }

    public function addSociedad($codMaterial, $organizacionVentas, $canalDist, $uniVenta,
                                $costoMedioVariable, $costoComercial, $desPeru, $aPedido,
                                $grupoMaterial, $marcaPropia, $importado, $afecto, $catComision, $codneg)
    {
        $sociedad = new Sociedad($codMaterial, $organizacionVentas, $canalDist, $uniVenta,
                                $costoMedioVariable, $costoComercial, $desPeru, $aPedido,
                                $grupoMaterial, $marcaPropia, $importado, $afecto, $catComision, $codneg);
        array_push($this->sociedades, $sociedad);
    }
    public function addImpuesto($pais, $impEspecifico, $clasifFiscal)
    {
        $impuesto = new Impuesto($pais, $impEspecifico, $clasifFiscal);
        array_push($this->impuestos, $impuesto);
    }
    public function addCentro($codCentro, $rotacion)
    {
        $centro = new Centro($codCentro, $rotacion);
        array_push($this->centros, $centro);
    }
}